package application.scraping;

/**
 * Created by Niki on 24/01/2018.
 */
public class MarketDetailsData {

  private String currencyName, currencyPair, currencyMarketName;

  private Long volume_24h;

  private Double price, volumeper;

  private Integer currencyMarketID;

  public void setCurrencyMarketName(String currencyMarketName){
    this.currencyMarketName = currencyMarketName;
  }

  public String getCurrencyMarketName(){
    return this.currencyMarketName;
  }

  public void setCurrencyName(String currencyName) {
    this.currencyName = currencyName;
  }

  public String getCurrencyName(){
    return this.currencyName;
  }

  public void setCurrencyMarketID(Integer currencyMarket){
    this.currencyMarketID = currencyMarket;
  }

  public Integer getCurrencyMarketID(){
    return this.currencyMarketID;
  }

  public void setCurrencyPair(String currencyPair){
    this.currencyPair = currencyPair;
  }

  public String getCurrencyPair(){
    return this.currencyPair;
  }

  public void setVolume_24h(Long volume_24h){
    this.volume_24h = volume_24h;
  }

  public Long getVolume_24h(){
    return this.volume_24h;
  }

  public void setPrice(Double price){
    this.price = price;
  }

  public Double getPrice(){
    return this.price;
  }

  public void setVolumeper(Double volumeper){
    this.volumeper = volumeper;
  }

  public Double getVolumeper(){
    return this.volumeper;
  }

}
