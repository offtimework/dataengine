package application.scraping;

import application.scraping.utils.WebFetcher;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CoinMarketCapScraper {

  private final String allCoinsUrl = "https://coinmarketcap.com/all/views/all/";

  @Autowired
  private WebFetcher webFetcher;

  public List<CoinMarketCapCurrency> scrapeIndexPage() {
    ;
    Document indexPage = webFetcher.getIndexPage(allCoinsUrl);

    if (indexPage == null) {
      return null;
    }

    return extractSnapshot(indexPage);
  }

  private List<CoinMarketCapCurrency> extractSnapshot(Document indexPage) {
    Element table = indexPage.select("table#currencies-all > tbody").get(0);

    return extractCurrencyFromTable(table);
  }

  private List<CoinMarketCapCurrency> extractCurrencyFromTable(Element table) {
    List<CoinMarketCapCurrency> result = new ArrayList<CoinMarketCapCurrency>();
    Elements rows = table.select("> tr");

    for (Element row : rows) {
      CoinMarketCapCurrency currency = castCurrency(row);
      result.add(currency);
    }

    return result;
  }

  private CoinMarketCapCurrency castCurrency(Element row) {
    CoinMarketCapCurrency currency = new CoinMarketCapCurrency();

    String ID = extractCurrencyID(row.select("span.currency-symbol > a").get(0).attr("href"));
    String name = convertIDToName(ID);
    currency.setName(name);
    currency.setWebsiteID(ID);

    String symbol = extractString(row.select("td.col-symbol"));
    currency.setSymbol(symbol != null ? symbol : "");

    Long marketCap = extractLong(row.select("td.market-cap"));
    currency.setMarketCap(marketCap != null ? marketCap : 0);

    Double price = extractDouble(row.select("a.price"));
    currency.setPrice(price != null ? price : 0);

    Long circulatingSupply = extractLong(row.select("td.circulating-supply"));
    currency.setCirculatingSupply(circulatingSupply != null ? circulatingSupply : 0);

    Long volume = extractLong(row.select("a.volume"));
    currency.setVolume_24h(volume != null ? volume : 0);

    Double movement_1h = extractDouble(row.select("td.percent-1h"));
    currency.setMovement_1h(movement_1h != null ? movement_1h : 0);

    Double movement24h = extractDouble(row.select("td.percent-24h"));
    currency.setMovement_24h(movement24h != null ? movement24h : 0);

    Double movement7d = extractDouble(row.select("td.percent-7d"));
    currency.setMovement_7d(movement7d != null ? movement7d : 0);

    return currency;
  }

  private String extractString(Elements data) {
    if (data.size() > 0) {
      return data.get(0).text();
    }

    return null;
  }

  private Long extractLong(Elements data) {
    if (data.size() > 0) {
      String text = data.get(0).text();
      if (text.contains("?") || text.contains("Low Vol")) {
        return null;
      }

      text = text.replace("$", "").replace(",", "").replace("*", "").replace(" ", "");

      try {
          if (text.contains(".")) {
              return Math.round(Double.parseDouble(text));
          }else{
              return Long.parseLong(text);
          }
      } catch (NumberFormatException e) {
        e.printStackTrace();
        return null;
      }
    }

    return null;
  }

  private Double extractDouble(Elements data) {
    if (data.size() > 0) {
      String text = data.get(0).text();
      if (text.contains("?")) {
        return null;
      }

      try {
        return Double.parseDouble(text.replace("$", "").replace(",", "")
            .replace(" ", "").replace("*", "").replace("%", ""));
      } catch (NumberFormatException e) {
        e.printStackTrace();
        return null;
      }
    }

    return null;
  }

  private String extractCurrencyID(String rawID){
    return rawID.replace("/", "").replace("currencies", "").trim();
  }

  private String convertIDToName(String ID){
    String[] fragments = ID.split("-");
    String result = "";

    for(String fragment: fragments){
      result = result + " " + fragment.substring(0,1).toUpperCase() + fragment.substring(1);
    }

    return result.trim();
  }

}
