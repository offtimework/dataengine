package application.scraping;

/**
 * Created by Niki on 23/01/2018.
 */
public class MarketsDetails {

  private String name, coinMarketURL;
  private Integer id;

  public MarketsDetails(){

  }

  public MarketsDetails(String name, String coinMarketURL){
    this.name = name;
    this.coinMarketURL = coinMarketURL;
  }

  public String getName(){
    return name;
  }

  public void setName(String name){
    this.name = name;
  }

  public String getCoinMarketURL() {
    return coinMarketURL;
  }

  public void setCoinMarketURL(String coinMarketURL) {
    this.coinMarketURL = coinMarketURL;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }
}
