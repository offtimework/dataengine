package application.scraping;

/**
 * Created by Niki on 09/01/2018.
 */
public class HistoricalPrices {

  private String date, coinName;
  private Integer coinID;
  private double open, high, low, close;
  private Long volume, marketCap;

  public Integer getCoinID() {
    return coinID;
  }

  public void setCoinID(Integer coinID){
    this.coinID = coinID;
  }

  public String getCoinName() {
    return coinName;
  }

  public void setCoinName(String coinName) {
    this.coinName = coinName;
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public double getOpen() {
    return open;
  }

  public void setOpen(Double open) {
    this.open = open;
  }

  public Double getHigh() {
    return high;
  }

  public void setHigh(Double high) {
    this.high = high;
  }

  public void setLow(Double low) {
    this.low = low;
  }

  public double getLow() {
    return low;
  }

  public void setClose(Double close) {
    this.close = close;
  }

  public double getClose() {
    return close;
  }

  public void setVolume(Long volume) {
    this.volume = volume;
  }

  public Long getVolume() {
    return volume;
  }

  public void setMarketCap(Long marketCap) {
    this.marketCap = marketCap;
  }

  public Long getMarketCap() {
    return marketCap;
  }

}
