package application.scraping;

import application.scraping.utils.WebFetcher;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Niki on 24/01/2018.
 */

@Component("marketdetailsdatascraper")
public class MarketDetailsDataScraper {

  @Autowired
  WebFetcher webFetcher;

  public List<MarketDetailsData> scrapeIndexPage(String linkToMarket){
    Document indexPage = webFetcher.getIndexPage(linkToMarket);

    String coinMarket = Arrays.asList(linkToMarket.split("/")).get(4);


    if(indexPage == null){return null;}
    if(indexPage.select("table#exchange-markets > tbody").size() == 0){
      return null;
    }

    return  extractSnapShot(indexPage, coinMarket);
  }

  private List<MarketDetailsData> extractSnapShot(Document indexPage, String coinMarket){
    Element table = indexPage.select("table#exchange-markets > tbody").get(0);
    return extractMarketDetailsFromTable(table, coinMarket);
  }

  private List<MarketDetailsData> extractMarketDetailsFromTable(Element table, String coinMarket){
    List<MarketDetailsData> result = new ArrayList<>();
    Elements rows = table.select(">tr");

    for (Element row : rows){
      MarketDetailsData market = getMarketDetailsData(row, coinMarket);
      result.add(market);
    }

    return result;
  }

  private MarketDetailsData getMarketDetailsData(Element row, String coinMarket){
    MarketDetailsData marketDetailsData = new MarketDetailsData();

    marketDetailsData.setCurrencyMarketName(coinMarket);

    Elements info = row.select("td");

    String coinName = extractString(info,1);
    marketDetailsData.setCurrencyName(coinName != null ? coinName : "");

    String pair = extractString(info,2);
    marketDetailsData.setCurrencyPair(pair != null ? pair : "");

    Long volume_24h = extractLong(info,3);
    marketDetailsData.setVolume_24h(volume_24h != null ? volume_24h : 0);

    Double price = extractDouble(info,4);
    marketDetailsData.setPrice(price != null ? price : 0);

    Double volumeper = extractDouble(info,5);
    marketDetailsData.setVolumeper(volumeper != null ? volumeper : 0);

    return marketDetailsData;
  }

  private String extractString(Elements data, Integer elementid){
    if(data.size() > 0){
      return data.get(elementid).text();
    }

    return null;
  }

  private Long extractLong(Elements data, Integer elementid){
    if(data.size() > 0){
      String text = data.get(elementid).text();
      if(text.equals("-")){return null;}

      try{
        return Long.parseLong(text.replace("$", "").replace(",", "").replace("*","").replace(" ", ""));
      }catch(NumberFormatException e){
        e.printStackTrace();
        return null;
      }
    }

    return null;
  }

  private Double extractDouble(Elements data, Integer elementid){
    if(data.size() > 0){
      String text = data.get(elementid).text();
      if(text.equals("-")){return null;}

      try{
        return Double.parseDouble(text.replace("$", "").replace(",", "").replace(" ", "").replace("*", "").replace("%", ""));
      }catch(NumberFormatException e){
        e.printStackTrace();
        return null;
      }
    }

    return null;
  }

}
