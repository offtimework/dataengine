package application.scraping;

public class CoinDetails {

    private int ID;

    private String name, description, symbol;

    public CoinDetails(){

    }

    public CoinDetails(String name, String description, String symbol){
        this.name = name;
        this.description = description;
        this.symbol = symbol;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSymbol() {return this.symbol;}

    public void setSymbol(String symbol) {this.symbol = symbol;}
}
