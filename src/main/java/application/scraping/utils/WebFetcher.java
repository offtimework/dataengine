package application.scraping.utils;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Component
public class WebFetcher {

    private HttpClient client;

    private BasicResponseHandler responseHandler;

    @PostConstruct
    private void init(){
        client = HttpClientBuilder.create().build();
        responseHandler = new BasicResponseHandler();
    }

    public Document getIndexPage(String url){
        HttpGet request = createRequest(url);
        String response = null;

        try{
            response = responseHandler.handleResponse(client.execute(request));
        }catch (IOException e){
            e.printStackTrace();
        }

        if(response == null){return null;}

        return Jsoup.parse(response);
    }

    private HttpGet createRequest(String url){
        HttpGet request = new HttpGet(url);
        return request;
    }
}
