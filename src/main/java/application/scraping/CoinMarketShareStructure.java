package application.scraping;

/**
 * Created by Niki on 10/01/2018.
 */
public class CoinMarketShareStructure {

  private String coinname, marketname, url, pair, volume_24h;


  private double price, volume_percent;

  public String getCoinname() {
    return coinname;
  }

  public void setCoinname(String coinname) {
    this.coinname = coinname;
  }

  public String getMarketname() {
    return marketname;
  }

  public void setMarketname(String marketname) {
    this.marketname = marketname;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getPair() {
    return pair;
  }

  public void setPair(String pair) {
    this.pair = pair;
  }

  public String getVolume_24h() {
    return volume_24h;
  }

  public void setVolume_24h(String volume_24h) {
    this.volume_24h = volume_24h;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public double getVolume_percent() {
    return volume_percent;
  }

  public void setVolume_percent(double volume_percent) {
    this.volume_percent = volume_percent;
  }

}
