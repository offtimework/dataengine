package application.scraping;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Niki on 10/01/2018.
 */

@Component
public class CoinMarketShareScraper {

  private final String currencyMarket = "https://coinmarketcap.com/currencies/volume/24-hour/";

  private HttpClient client;

  private BasicResponseHandler responseHandler;

  @PostConstruct
  private void init(){
    client = HttpClientBuilder.create().build();
    responseHandler = new BasicResponseHandler();
  }

  public List<CoinMarketShareStructure> scrapeIndexPage(){
    List<CoinMarketShareStructure> currencies = new ArrayList<CoinMarketShareStructure>();
    Document indexPage = getIndexPage(currencyMarket);

    if(indexPage == null){return null;}

    currencies = extractSnapshot(indexPage);

    return currencies;
  }

  private List<CoinMarketShareStructure> extractSnapshot(Document indexPage){
    Element table = indexPage.select("table > tbody").get(0);

    return extractDataFromTable(table);
  }


  private List<CoinMarketShareStructure> extractDataFromTable (Element table){
    List<CoinMarketShareStructure> result = new ArrayList<CoinMarketShareStructure>();
    Elements rows = table.select(">tr");

    for (Element row: rows){
      CoinMarketShareStructure coins = getData(row);
      result.add(coins);
    }

    return result;
  }

  private CoinMarketShareStructure getData(Element row){
    CoinMarketShareStructure coinShare = new CoinMarketShareStructure();

    Elements info = row.select("td");

    if(info.select("a") != null){
      if(info.select("a").attr("href").toString().contains("exchanges")){
        String marketName = info.select("a").first().text().toString();
        coinShare.setMarketname(marketName);
      }
      if(info.select("a").attr("target").toString().contains("blank")){
        String pair = info.select("a").get(1).text().toString();
        coinShare.setPair(pair);
      }
    }
    String volume = row.select("td.volume").text().toString();
    coinShare.setVolume_24h(volume != null ? volume : "");

    return coinShare;
  }

  private String extractString(Elements data, Integer elementid){
    if(data.size() > 0){
      return data.get(elementid).text();
    }

    return null;
  }

  private Long extractLong(Elements data){
    if(data.size() > 0){
      String text = data.get(0).text();
      if(text.equals("?")){return null;}

      try{
        return Long.parseLong(text.replace("$", "").replace(",", ""));
      }catch(NumberFormatException e){
        e.printStackTrace();
        return null;
      }
    }

    return null;
  }

  private Double extractDouble(Elements data){
    if(data.size() > 0){
      String text = data.get(0).text();
      if(text.equals("?")){return null;}

      try{
        return Double.parseDouble(text.replace("$", "").replace(",", "").replace(" ", "").replace("*", "").replace("%", ""));
      }catch(NumberFormatException e){
        e.printStackTrace();
        return null;
      }
    }

    return null;
  }


  private Document getIndexPage(String url){
    HttpGet request = createRequest(url);
    String response = null;

    try{
      response = responseHandler.handleResponse(client.execute(request));
    }catch (IOException e){
      e.printStackTrace();
    }

    if(response == null){return null;}

    return Jsoup.parse(response);
  }

  private HttpGet createRequest(String url){
    HttpGet request = new HttpGet(url);
    return request;
  }


}
