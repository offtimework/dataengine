package application.scraping;

/**
 * Created by Niki on 24/01/2018.
 */
public class CurrencyPairs {

  private Integer ID;

  private String currencyPair;

  public CurrencyPairs(){

  }

  public CurrencyPairs(String currencyPair, Integer ID){
    this.currencyPair = currencyPair;
    this.ID  = ID;
  }

  public String getCurrencyPair(){
    return this.currencyPair;
  }

  public void setCurrencyPair(String currencyPair){
    this.currencyPair = currencyPair;
  }

  public Integer getID(){
    return this.ID;
  }

  public void setID(Integer ID){
    this.ID = ID;
  }

}
