package application.scraping;

import application.dataFeed.DataOutput;
import application.scraping.utils.WebFetcher;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Niki on 09/01/2018.
 */

@Component
public class HistoricalPricesScraper {

  @Autowired
  private WebFetcher webFetcher;

  @Autowired
  DataOutput dataOutput;

  DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
  Date date = new Date();


  public List<HistoricalPrices> scrapeIndexPage(String currency, String startdate){
    String historicalURL = "https://coinmarketcap.com/currencies/" + currency + "/historical-data/?start=" + startdate + "&end=" + dateFormat.format(date);

    Document indexPage = webFetcher.getIndexPage(historicalURL);

    if(indexPage == null){return null;}

    return extractSnapshot(indexPage, currency);
  }

  private List<HistoricalPrices> extractSnapshot(Document indexPage, String currency){
    Element table = indexPage.select("table").first();

    return extractCurrencyFromTable(table, currency);
  }

  private List<HistoricalPrices> extractCurrencyFromTable(Element table, String currencyY){
    List<HistoricalPrices> result = new ArrayList<HistoricalPrices>();
    Elements rows = table.select("tr.text-right");

    for(Element row: rows){
      HistoricalPrices currency = getHistPrices(row, currencyY);
      result.add(currency);
    }

    return result;
  }

  private HistoricalPrices getHistPrices(Element row, String currency){
    HistoricalPrices histprices = new HistoricalPrices();

    histprices.setCoinName(currency);
    histprices.setCoinID(dataOutput.getCoinPerName(currency));

    Elements info = row.select("td");

    String date = extractString(info,0);
    histprices.setDate(date != null ? date : "");

    Double open = extractDouble(info, 1);
    histprices.setOpen(open != null ? open : 0);

    Double high = extractDouble(info, 2);
    histprices.setHigh(high != null ? high : 0);

    Double low = extractDouble(info, 3);
    histprices.setLow(low != null ? low: 0);

    Double close = extractDouble(info, 4);
    histprices.setClose(close != null ? close : 0);

    Long volume = extractLong(info, 5);
    histprices.setVolume(volume != null ? volume: 0);

    Long marketCap = extractLong(info, 6);
    histprices.setMarketCap(marketCap != null ? marketCap: 0);

    return histprices;

  }




  private String extractString(Elements data, Integer elementid){
    if(data.size() > 0){
      return data.get(elementid).text();
    }

    return null;
  }

  private Long extractLong(Elements data, Integer elementid){
    if(data.size() > 0){
      String text = data.get(elementid).text();
      if(text.equals("-")){return null;}

      try{
        return Long.parseLong(text.replace("$", "").replace(",", ""));
      }catch(NumberFormatException e){
        e.printStackTrace();
        return null;
      }
    }

    return null;
  }

  private Double extractDouble(Elements data, Integer elementid){
    if(data.size() > 0){
      String text = data.get(elementid).text();
      if(text.equals("-")){return null;}

      try{
        return Double.parseDouble(text.replace("$", "").replace(",", "").replace(" ", "").replace("*", "").replace("%", ""));
      }catch(NumberFormatException e){
        e.printStackTrace();
        return null;
      }
    }

    return null;
  }
}
