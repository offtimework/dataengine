package application.scraping;

import application.scraping.utils.WebFetcher;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Niki on 23/01/2018.
 */

@Component("marketDetailsScraper")
public class MarketDetailsScraper {

  @Autowired
  WebFetcher webFetcher;

  public List<MarketsDetails> scrapeIndexPage(String coin){
    String marketUrl = "https://coinmarketcap.com/currencies/" + coin + "/#markets";

    Document indexPage = webFetcher.getIndexPage(marketUrl);

    if(indexPage == null){return null;}
    if(indexPage.select("table#markets-table > tbody").size() == 0){
      return null;
    }
    return extractSnapShot(indexPage);
  }

  private List<MarketsDetails> extractSnapShot(Document indexPage){
    Element table = indexPage.select("table#markets-table > tbody").get(0);
    return extractMarketDetailsFromTable(table);
  }

  private List<MarketsDetails> extractMarketDetailsFromTable(Element table){
    List<MarketsDetails> result = new ArrayList<>();
    Elements rows = table.select("> tr");

    for(Element row : rows){
      MarketsDetails market = getMarketDetails(row);
      result.add(market);
    }

    return result;

  }


  private MarketsDetails getMarketDetails(Element row){
    MarketsDetails marketsDetails = new MarketsDetails();

    Elements info = row.select("td");

    String market = extractString(info, 1);
    marketsDetails.setName(market != null ? market : null);

    String marketUrl = extractURL(info, 1);
    marketsDetails.setCoinMarketURL(marketUrl != null ? "https://coinmarketcap.com" + marketUrl : null);

    return marketsDetails;
  }

  private String extractString(Elements data, Integer elementid){
    if(data.size() > 0){
      return data.get(elementid).text();
    }

    return null;
  }

  private String extractURL(Elements data, Integer elementid){
    if(data.size() > 0){
      return data.get(elementid).select("a").attr("href");
    }

    return null;
  }






}
