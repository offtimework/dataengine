package application.scraping;

public class CoinMarketCapCurrency {

    private String name;
    private String symbol;

    private String websiteID;

    private long marketCap, volume_24h, circulatingSupply;

    private double price, movement_1h, movement_24h, movement_7d;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public long getMarketCap() {
        return marketCap;
    }

    public void setMarketCap(long marketCap) {
        this.marketCap = marketCap;
    }

    public long getCirculatingSupply() {
        return circulatingSupply;
    }

    public void setCirculatingSupply(long circulatingSupply) {
        this.circulatingSupply = circulatingSupply;
    }

    public long getVolume_24h() {
        return volume_24h;
    }

    public void setVolume_24h(long volume_24h) {
        this.volume_24h = volume_24h;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getMovement_1h() {
        return movement_1h;
    }

    public void setMovement_1h(double movement_1h) {
        this.movement_1h = movement_1h;
    }

    public double getMovement_24h() {
        return movement_24h;
    }

    public void setMovement_24h(double movement_24h) {
        this.movement_24h = movement_24h;
    }

    public double getMovement_7d() {
        return movement_7d;
    }

    public void setMovement_7d(double movement_7d) {
        this.movement_7d = movement_7d;
    }

    public String getWebsiteID() {
        return websiteID;
    }

    public void setWebsiteID(String websiteID) {
        this.websiteID = websiteID;
    }

}
