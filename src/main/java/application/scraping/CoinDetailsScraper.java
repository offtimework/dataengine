package application.scraping;

import application.scraping.utils.WebFetcher;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CoinDetailsScraper {

    private final String allCoinsUrl = "https://coinmarketcap.com/all/views/all/";

    @Autowired
    private WebFetcher webFetcher;

    public List<CoinDetails> scrapeIndexPage(){
        List<CoinDetails> result = new ArrayList<>();
        Document indexPage = webFetcher.getIndexPage(allCoinsUrl);

        if(indexPage == null){return null;}

        result = extractDetails(indexPage);

        return  result;
    }

    private List<CoinDetails> extractDetails(Document indexPage){
        Element table = indexPage.select("table#currencies-all > tbody").get(0);

        return extractDetailsFromTable(table);
    }

    private List<CoinDetails> extractDetailsFromTable(Element table){
        List<CoinDetails> result = new ArrayList<>();
        Elements rows = table.select("> tr");

        for(Element row: rows){
            CoinDetails details = castDetails(row);
            result.add(details);
        }

        return result;
    }

    private CoinDetails castDetails(Element element){
        String ID = extractCurrencyID(element.select("span.currency-symbol > a").get(0).attr("href"));
        String name = convertIDToName(ID);
        String symbol =  extractString(element.select("td.col-symbol"));
        return new CoinDetails(ID, name, symbol);
    }

    private String extractCurrencyID(String rawID){
        return rawID.replace("/", "").replace("currencies", "").trim();
    }

    private String convertIDToName(String ID){
        String[] fragments = ID.split("-");
        String result = "";

        for(String fragment: fragments){
            result = result + " " + fragment.substring(0,1).toUpperCase() + fragment.substring(1);
        }

        return result.trim();
    }

    private String extractString(Elements data) {
        if (data.size() > 0) {
            return data.get(0).text();
        }

        return null;
    }

}
