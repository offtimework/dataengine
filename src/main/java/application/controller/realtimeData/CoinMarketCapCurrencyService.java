package application.controller.realtimeData;

import application.scraping.CoinMarketCapCurrency;
import application.scraping.CoinMarketCapScraper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CoinMarketCapCurrencyService {

    @Autowired
    private CoinMarketCapCurrencyRepository repository;

    @Autowired
    private CoinMarketCapScraper scraper;

    public List<CoinMarketCapCurrency> getData(){return repository.getCurrencies();}

    public List<CoinMarketCapCurrency> extractSnapshot(){return scraper.scrapeIndexPage();}
}
