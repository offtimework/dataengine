package application.controller.realtimeData;

import application.dataFeed.util.CoinMarketCapCurrencyResultSetExtractor;
import application.scraping.CoinMarketCapCurrency;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CoinMarketCapCurrencyRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<CoinMarketCapCurrency> getCurrencies(){
        return jdbcTemplate.query("SELECT * FROM PriceData_5min", new CoinMarketCapCurrencyResultSetExtractor());
    }
}
