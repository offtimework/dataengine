package application.controller.coinDetails;

import application.scraping.CoinDetails;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Niki on 17/01/2018.
 */

@Service
@JsonSerialize
public class CoinDetailsService {

  @Autowired
  private CoinDetailsRepository coinDetailsRepository;

  List<CoinDetails> fetchDetails(){
    List<CoinDetails> coinDetails = coinDetailsRepository.fetchDetails();
    return  coinDetails;
  }
}
