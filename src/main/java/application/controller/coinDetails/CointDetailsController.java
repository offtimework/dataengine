package application.controller.coinDetails;

import application.scraping.CoinDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Niki on 17/01/2018.
 */

@RestController
public class CointDetailsController {

  @Autowired
  CoinDetailsService coinDetailsService;

  @GetMapping(value = "/getcoindetails")
  List<CoinDetails> fetchDetails(){
    return coinDetailsService.fetchDetails();
  }
}
