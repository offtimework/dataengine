package application.controller.coinDetails;

import application.dataFeed.util.CoinDetailsRowMapper;
import application.scraping.CoinDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Niki on 16/01/2018.
 */
@Component
public class CoinDetailsRepository {
  @Autowired
  private JdbcTemplate jdbcTemplate;

  public List<CoinDetails> fetchDetails(){
    String sql = "SELECT id, coinName, description = descirption FROM coin";
    return jdbcTemplate.query(sql, new CoinDetailsRowMapper());
  }
}
