package application.controller;

import application.dataFeed.DataInput;
import application.dataFeed.DataOutput;
import application.scraping.*;
import com.sun.corba.se.spi.ior.ObjectKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.crypto.Data;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

@Component
@RestController
public class CoreController {

  @Autowired
  private CoinMarketCapScraper scraper;

  @Autowired
  private HistoricalPricesScraper histscraper;

  @Autowired
  private CoinDetailsScraper coinDetailsScraper;

  @Autowired
  private MarketDetailsScraper marketDetailsScraper;

  @Autowired
  private MarketDetailsDataScraper marketDetailsDataScraper;

  @Autowired
  private DataInput dataInput;

  @Autowired
  private DataOutput dataOutput;

  @RequestMapping(value = "/snapshot")
  public String getSnapshot() {
    String result = "";

    List<CoinMarketCapCurrency> currencies = scraper.scrapeIndexPage();

    for (CoinMarketCapCurrency currency : currencies) {
      result = result + currency.getName() + " " + currency.getSymbol() + " "
          + currency.getMarketCap() + " " + currency.getPrice() + " "
          + currency.getCirculatingSupply() + " " + currency.getVolume_24h() + " "
          + currency.getMovement_1h() + " " + currency.getMovement_24h() + " "
          + currency.getMovement_7d() + " \n\n";
    }

    return result;
  }

  @RequestMapping(value = "/histprices/{coin}/{startdate}")
  public String getHistory(@PathVariable("coin") String coin,
                           @PathVariable("startdate") String startdate) {
    String result = "";

    List<HistoricalPrices> currencies = histscraper.scrapeIndexPage(coin, startdate);

    for (HistoricalPrices currency : currencies) {
      result = result + currency.getDate() + " " + currency.getOpen() + " "
          + currency.getHigh() + " " + currency.getLow() + " "
          + currency.getClose() + " " + currency.getVolume() + " "
          + currency.getMarketCap() + " " + "\n\n";
    }

    return result;
  }


  @RequestMapping(value = "/list-coin-ids")
  public String getIds() {

    DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    Calendar cal = Calendar.getInstance();
    String result = "";

    List<CoinDetails> coinDetailsList = coinDetailsScraper.scrapeIndexPage();

    for (CoinDetails coinDetails : coinDetailsList) {
      result = result + "Name: " + coinDetails.getName() + "\n" +
          "Description: " + coinDetails.getDescription() + "\n" +
          "Historical data: " + "https://coinmarketcap.com/currencies/" + coinDetails.getName() +
          "/historical-data/?start=20130428&end=" + dateFormat.format(cal) + "\n\n";
    }


    return result;
  }

  @RequestMapping(value = "/insert-coin-details")
  public String insertCoindDetails() {
    dataInput.insertCoinDetails(coinDetailsScraper.scrapeIndexPage());
    return "Insertion finished successfully";
  }

  @RequestMapping(value = "/insertHistoricaldata/{startdate}")
  public String insertHistoricalData(@PathVariable("startdate") String startdate) {
    List<Map<String, Object>> currencies = dataOutput.getCointDetails();
    for (Map<String, Object> map : currencies) {
      for (Map.Entry<String, Object> entry : map.entrySet()) {
        if (entry.getKey().contains("coin")) {
          List<HistoricalPrices> curr = histscraper.scrapeIndexPage(entry.getValue().toString(), startdate);
          dataInput.insertHistoricalPrices(curr);
        }
      }
    }
    return "End";
  }

  @RequestMapping(value = "/getMarkets/{coin}")
  public String getMarkets(@PathVariable("coin") String coin) {
    String result = "";

    List<MarketsDetails> markets = marketDetailsScraper.scrapeIndexPage(coin);

    for (MarketsDetails market : markets) {
      result = result + market.getName() + " " + market.getCoinMarketURL();
    }

    dataInput.insertMarketDetails(markets);
    return result;
  }

  @RequestMapping(value = "/getMarketData")
  public String getMarketsData(){
    String result = "";
    List<MarketDetailsData> market = new ArrayList<>();
    List<Map<String,Object>> markets = dataOutput.getMarketsData();

    for (Map<String, Object> map : markets) {
      for (Map.Entry<String, Object> entry : map.entrySet()) {
        System.out.println(map.get("id"));
        Integer marketID = Integer.parseInt(map.get("id").toString());
        if (entry.getKey().contains("coinMarket")) {
          System.out.println(entry.getValue());
          List<MarketDetailsData> curr = marketDetailsDataScraper.scrapeIndexPage(entry.getValue().toString());
          for(int i = 0;i<curr.size();i++){
            System.out.println(curr.get(i).getCurrencyName());
            Integer coinID = dataOutput.getCoinPerDescription(curr.get(i).getCurrencyName());
            if(coinID != null){
              curr.get(i).setCurrencyMarketID(marketID);
              market.add(curr.get(i));
            }
          }
        }
      }
    }
    dataInput.insertRealTimeMarketsData(market);
    return "End";
  }

}