package application.dataFeed.scheduledTasks;

import application.dataFeed.DataInput;
import application.dataFeed.DataOutput;
import application.scraping.HistoricalPrices;
import application.scraping.HistoricalPricesScraper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Niki on 23/01/2018.
 */
@Component
public class HistoricalDataScheduledTasks {

  static Logger log = LoggerFactory.getLogger(HistoricalDataScheduledTasks.class);

  @Autowired
  private DataInput dataInput;

  @Autowired
  private DataOutput dataOutput;

  @Autowired
  private HistoricalPricesScraper historicalPricesScraper;

  @Scheduled(cron = "0 0 8 * * *")
  public void transferLatestDaydata(){
    long startTime = System.nanoTime();
    log.info("Transfer of last day history has started.");
    List<Map<String, Object>> currencies = dataOutput.getCointDetails();
    for (Map<String, Object> map : currencies) {
      for (Map.Entry<String, Object> entry : map.entrySet()) {
        if (entry.getKey().contains("coin")) {
          List<HistoricalPrices> curr = historicalPricesScraper.scrapeIndexPage(entry.getValue().toString(), getDate(-1));
          dataInput.insertHistoricalPrices(curr);
        }
      }
    }
    double duration = (System.nanoTime()-startTime)/1e6;
    log.info("Transfer of last day history data has been completed.\n"+
    "Transfer time: " + duration + " seconds.");
  }

  private String getDate(int dayDifference){
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new Date());
    calendar.add(Calendar.DATE, dayDifference);
    return  new SimpleDateFormat("yyyyMMdd").format(calendar.getTime());
  }
}
