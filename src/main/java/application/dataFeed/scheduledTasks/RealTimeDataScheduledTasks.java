package application.dataFeed.scheduledTasks;

import application.controller.realtimeData.CoinMarketCapCurrencyService;
import application.dataFeed.DataInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class RealTimeDataScheduledTasks {

    static Logger log = LoggerFactory.getLogger(RealTimeDataScheduledTasks.class);

    @Autowired
    private DataInput dataInput;

    @Autowired
    private CoinMarketCapCurrencyService currencyService;

    @Scheduled(fixedDelay = 1000*60*5)
    public void transferSnapshot(){
        long startTime = System.nanoTime();
        log.info("Snapshot extraction has started.");
        dataInput.insertRealTimePrice(currencyService.extractSnapshot());
        double duration = (System.nanoTime() - startTime)/1e9;
        log.info("Snapshot transferred to database in " + duration + "seconds.");
    }
}
