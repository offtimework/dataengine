package application.dataFeed.scheduledTasks;

import application.dataFeed.DataInput;
import application.dataFeed.DataOutput;
import application.scraping.MarketDetailsData;
import application.scraping.MarketDetailsDataScraper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Niki on 24/01/2018.
 */
@Component
public class MarketDetailsDataScheduledTasks {

  static Logger log = LoggerFactory.getLogger(MarketDetailsDataScheduledTasks.class);

  @Autowired
  private DataInput dataInput;

  @Autowired
  private DataOutput dataOutput;

  @Autowired
  private MarketDetailsDataScraper marketDetailsDataScraper;

  @Scheduled(cron="0 0 4 * * MON-FRI")
  public void getMarketsData(){
    long startTime = System.nanoTime();
    log.info("Snapshot extraction has started.");
    List<MarketDetailsData> market = new ArrayList<>();
    List<Map<String,Object>> markets = dataOutput.getMarketsData();

    for (Map<String, Object> map : markets) {
      for (Map.Entry<String, Object> entry : map.entrySet()) {
        Integer marketID = Integer.parseInt(map.get("id").toString());
        if (entry.getKey().contains("coinMarket")) {
          List<MarketDetailsData> curr = marketDetailsDataScraper.scrapeIndexPage(entry.getValue().toString());
          for(int i = 0;i<curr.size();i++){
            Integer coinID = dataOutput.getCoinPerDescription(curr.get(i).getCurrencyName());
            if(coinID != null){
              curr.get(i).setCurrencyMarketID(marketID);
              market.add(curr.get(i));
            }
          }
        }
      }
    }
    dataInput.insertRealTimeMarketsData(market);
    double duration = (System.nanoTime()-startTime)/1e6;
    log.info("Transfer of latest market data details info has been completed.\n" +
        "Transfer time: " + duration + " seconds.");
  }
}

