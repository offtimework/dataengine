package application.dataFeed.scheduledTasks;

import application.dataFeed.DataInput;
import application.dataFeed.DataOutput;
import application.scraping.MarketDetailsScraper;
import application.scraping.MarketsDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * Created by Niki on 23/01/2018.
 */
@Component
public class MarketDetailsScheduledTasks {
  static Logger log = LoggerFactory.getLogger(MarketDetailsScheduledTasks.class);

  @Autowired
  private DataInput dataInput;

  @Autowired
  private DataOutput dataOutput;

  @Autowired
  private MarketDetailsScraper marketDetailsScraper;

  //@Scheduled(cron="0 0/20 * * * MON-FRI")
  public void transferMarketDetails(){
    long startTime = System.nanoTime();
    log.info("Snapshot extraction has started.");
    List<Map<String, Object>> currencies = dataOutput.getCointDetails();
    for (Map<String, Object> currency : currencies) {
      for (Map.Entry<String, Object> entry : currency.entrySet()) {
        if (entry.getKey().contains("coin")) {
          List<MarketsDetails> marketsDetails = marketDetailsScraper.scrapeIndexPage(entry.getValue().toString());
          dataInput.insertMarketDetails(marketsDetails);
        }
      }
    }
    double duration = (System.nanoTime()-startTime)/1e6;
    log.info("Transfer of latest market details info has been completed.\n" +
        "Transfer time: " + duration + " seconds.");
  }
}
