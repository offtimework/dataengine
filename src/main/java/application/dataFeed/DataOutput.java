package application.dataFeed;

import application.dataFeed.util.CoinDetailsRowMapper;
import application.dataFeed.util.CurrencyPairRowMapper;
import application.dataFeed.util.MarketDetailsMapper;
import application.scraping.CoinDetails;
import application.scraping.CurrencyPairs;
import application.scraping.MarketsDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class DataOutput {
  @Autowired
  private JdbcTemplate jdbcTemplate;

  public List<Map<String,Object>> getCointDetails(){
    List<Map<String, Object>> ids = jdbcTemplate.queryForList("SELECT id, coinName FROM coin");
    return ids;
  }

  public Integer getCoinPerName(String currencyName){
    CoinDetails details;
    try{
      details = jdbcTemplate.queryForObject("SELECT * from coin where coinName = ?", new Object[] {currencyName}, new CoinDetailsRowMapper());
    }catch(EmptyResultDataAccessException e){
      return null;
    }
    return details.getID();
  }

  public Integer getCoinPerDescription(String currencyDescription){
    CoinDetails details;
    try{
      details = jdbcTemplate.queryForObject("SELECT * from coin where descirption = ?", new Object[] {currencyDescription}, new CoinDetailsRowMapper());
    }catch (EmptyResultDataAccessException e){
      return null;
    }

    return details.getID();
  }

  public Integer getMarketPerName(String marketName){
    MarketsDetails marketsDetails;
    try{
      marketsDetails = jdbcTemplate.queryForObject("SELECT * FROM market where LOWER(name) = ?", new Object[]{marketName}, new MarketDetailsMapper());
    }catch (EmptyResultDataAccessException e){
      return null;
    }
    return marketsDetails.getId();
  }

  public List<Map<String, Object>> getMarketsData(){
    List<Map<String, Object>> marketsData = jdbcTemplate.queryForList("SELECT id, coinMarketURL, name FROM market");
    return marketsData;
  }

  public Integer getCurrencyPairPerName(String currencyPair){
    CurrencyPairs currencyPairs;

    try{
      currencyPairs = jdbcTemplate.queryForObject("SELECT * FROM currencyPair where currencyPair = ?", new Object[]{currencyPair}, new CurrencyPairRowMapper());
    }catch (EmptyResultDataAccessException e){
      return null;
    }
    return currencyPairs.getID();
  }

  public Integer getCoinBySymbol(String symbol){
    CoinDetails currency = jdbcTemplate.queryForObject("SELECT * from coin where Symbol = ?", new Object[] {symbol}, new CoinDetailsRowMapper());
    return currency.getID();
  }

  public String getCoinDetailsPerCoinId(Integer coinID){
    CoinDetails coinDetails = jdbcTemplate.queryForObject("SELECT * FROM coin where id = ?", new Object[] {coinID}, new CoinDetailsRowMapper());
    return coinDetails.getName();
  }
}
