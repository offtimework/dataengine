package application.dataFeed.util;

import application.scraping.HistoricalPrices;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class HistoricalPricesRowMapper implements RowMapper<HistoricalPrices> {

    @Override
    public HistoricalPrices mapRow(ResultSet rs, int rowNum) throws SQLException{
        HistoricalPrices prices = new HistoricalPrices();
        prices.setCoinID(rs.getInt("refID_coin"));
        prices.setCoinName(rs.getString("coinName"));
        prices.setDate(rs.getString("date"));
        prices.setOpen(rs.getDouble("openPrice"));
        prices.setClose(rs.getDouble("closePrice"));
        prices.setHigh(rs.getDouble("highPrice"));
        prices.setLow(rs.getDouble("lowPrice"));
        prices.setVolume(rs.getLong("volume"));
        prices.setMarketCap(rs.getLong("mcap"));

        return prices;
    }
}
