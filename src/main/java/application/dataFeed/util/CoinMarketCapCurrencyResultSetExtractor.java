package application.dataFeed.util;

import application.scraping.CoinMarketCapCurrency;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CoinMarketCapCurrencyResultSetExtractor implements ResultSetExtractor<List<CoinMarketCapCurrency>> {

    @Override
    public List<CoinMarketCapCurrency> extractData(ResultSet rs) throws SQLException{
        List<CoinMarketCapCurrency> result = new ArrayList<>();

        while(rs.next()){
            CoinMarketCapCurrency currency = new CoinMarketCapCurrency();
            currency.setMarketCap(rs.getLong("MarketCap"));
            currency.setCirculatingSupply(rs.getLong("CirculatingSupply"));
            currency.setPrice(rs.getDouble("Price"));
            currency.setVolume_24h(rs.getLong("Volume24h"));
            currency.setMovement_1h(rs.getDouble("Movement1h"));
            currency.setMovement_24h(rs.getDouble("Movement24h"));
            currency.setMovement_7d(rs.getDouble("Movement7d"));

            result.add(currency);
        }

        return result;
    }
}
