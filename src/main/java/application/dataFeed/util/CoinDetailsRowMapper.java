package application.dataFeed.util;


import application.scraping.CoinDetails;
import application.scraping.CoinMarketCapCurrency;
import org.springframework.jdbc.core.RowMapper;


import java.sql.ResultSet;
import java.sql.SQLException;

public class CoinDetailsRowMapper implements RowMapper<CoinDetails> {

   @Override
    public CoinDetails mapRow(ResultSet rs, int rowNum) throws SQLException{
       CoinDetails details = new CoinDetails();

       details.setID(rs.getInt("ID"));
       details.setName(rs.getString("coinname"));
       details.setSymbol(rs.getString("descirption"));
       details.setSymbol(rs.getString("Symbol"));

       return details;
   }
}
