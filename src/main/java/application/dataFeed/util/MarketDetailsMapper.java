package application.dataFeed.util;


import application.scraping.MarketsDetails;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Niki on 23/01/2018.
 */
public class MarketDetailsMapper implements RowMapper<MarketsDetails>{

  @Override
  public MarketsDetails mapRow(ResultSet rs, int rowNum) throws SQLException{
    MarketsDetails marketsDetails = new MarketsDetails();
    marketsDetails.setName(rs.getString("name"));
    marketsDetails.setCoinMarketURL(rs.getString("coinMarketURL"));
    marketsDetails.setId(rs.getInt("ID"));

    return  marketsDetails;
  }
}
