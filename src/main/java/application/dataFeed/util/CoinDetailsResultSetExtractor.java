package application.dataFeed.util;

import application.scraping.CoinDetails;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CoinDetailsResultSetExtractor implements ResultSetExtractor<List<CoinDetails>> {

    @Override
    public List<CoinDetails> extractData(ResultSet rs) throws SQLException{
        List<CoinDetails> result = new ArrayList<>();

        while(rs.next()){
            CoinDetails details = new CoinDetails();
            details.setID(rs.getInt("ID"));
            details.setName(rs.getString("coinName"));
            details.setSymbol(rs.getString("Symbol"));

            result.add(details);
        }

        return result;
    }
}
