package application.dataFeed.util;

import application.scraping.CurrencyPairs;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Niki on 24/01/2018.
 */
public class CurrencyPairRowMapper implements RowMapper<CurrencyPairs>{

  @Override
  public CurrencyPairs mapRow(ResultSet rs, int rowNum) throws SQLException{
    CurrencyPairs currencyPairs = new CurrencyPairs();

    currencyPairs.setCurrencyPair(rs.getString("currencyPair"));
    currencyPairs.setID(rs.getInt("ID"));

    return currencyPairs;
  }
}
