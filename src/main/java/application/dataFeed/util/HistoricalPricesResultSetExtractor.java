package application.dataFeed.util;

import application.scraping.HistoricalPrices;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class HistoricalPricesResultSetExtractor implements ResultSetExtractor<List<HistoricalPrices>> {

    @Override
    public List<HistoricalPrices> extractData(ResultSet rs) throws SQLException{
        List<HistoricalPrices> result = new ArrayList<>();

        while(rs.next()){
            HistoricalPrices prices = new HistoricalPrices();
            prices.setOpen(rs.getDouble("openPrice"));
            prices.setClose(rs.getDouble("closePrice"));
            prices.setHigh(rs.getDouble("highPrice"));
            prices.setLow(rs.getDouble("lowPrice"));
            prices.setVolume(rs.getLong("volume"));

            result.add(prices);
        }

        return result;
    }
}
