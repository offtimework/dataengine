package application.dataFeed.util;

import application.scraping.CoinMarketCapCurrency;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CoinMarketCapRowMapper implements RowMapper<CoinMarketCapCurrency> {

    @Override
    public CoinMarketCapCurrency mapRow(ResultSet rs, int rowNum) throws SQLException{
        CoinMarketCapCurrency currency = new CoinMarketCapCurrency();
        currency.setMarketCap(rs.getLong("MarketCap"));
        currency.setCirculatingSupply(rs.getLong("CirculatingSupply"));
        currency.setPrice(rs.getDouble("Price"));
        currency.setVolume_24h(rs.getLong("Volume24h"));
        currency.setMovement_1h(rs.getDouble("Movement1h"));
        currency.setMovement_24h(rs.getDouble("Movement24h"));
        currency.setMovement_7d(rs.getDouble("Movement7d"));

        return currency;
    }
}
