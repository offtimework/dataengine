package application.dataFeed;

import application.scraping.*;
import org.hibernate.validator.constraints.LuhnCheck;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

@Component
public class DataInput {

  @Autowired
  private JdbcTemplate jdbcTemplate;

  @Autowired
  private DataOutput dataOutput;

  public void insertCoinDetails(List<CoinDetails> coinDetails) {
    jdbcTemplate.batchUpdate("INSERT INTO Coin (coinName, descirption) values (?,?)", new BatchPreparedStatementSetter() {

      @Override
      public void setValues(PreparedStatement ps, int i) throws SQLException {
        CoinDetails coinDetail = coinDetails.get(i);
        ps.setString(1, coinDetail.getName());
        ps.setString(2, coinDetail.getDescription());
      }

      @Override
      public int getBatchSize() {
        return coinDetails.size();
      }
    });
  }

  public int insertMarketDetail(MarketsDetails marketsDetails) {
    String sql = "INSERT INTO Market (name, coinMarketURL) VALUES (?,?)";
    final PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
      @Override
      public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
        final PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

        preparedStatement.setString(1, marketsDetails.getName());
        preparedStatement.setString(2, marketsDetails.getCoinMarketURL());
        return preparedStatement;
      }
    };
    final KeyHolder keyHolder = new GeneratedKeyHolder();

    jdbcTemplate.update(preparedStatementCreator, keyHolder);
    return keyHolder.getKey().intValue();
  }

  public int insertCurrencyPairData(String currencyPair){
    String sql = "INSERT INTO currencyPair (currencyPair) VALUES (?)";
    final PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
      @Override
      public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
        final PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

        preparedStatement.setString(1, currencyPair);
        return preparedStatement;
      }
    };

    final KeyHolder keyHolder = new GeneratedKeyHolder();

    jdbcTemplate.update(preparedStatementCreator, keyHolder);
    return keyHolder.getKey().intValue();
  }

  public void insertHistoricalPrices(List<HistoricalPrices> historicalPrices) {
    jdbcTemplate.batchUpdate("INSERT INTO priceData_Hist (date, refID_coin, openPrice, closePrice, highPrice, lowPrice, volume, mcap) VALUES (?, ?, ?, ?, ?, ?, ?, ?)", new BatchPreparedStatementSetter() {
      @Override
      public void setValues(PreparedStatement ps, int i) throws SQLException {
        HistoricalPrices historicalPrice = historicalPrices.get(i);
        ps.setString(1, historicalPrice.getDate());
        ps.setInt(2, historicalPrice.getCoinID());
        ps.setDouble(3, historicalPrice.getOpen());
        ps.setDouble(4, historicalPrice.getClose());
        ps.setDouble(5, historicalPrice.getHigh());
        ps.setDouble(6, historicalPrice.getLow());
        ps.setLong(7, historicalPrice.getVolume());
        ps.setLong(8, historicalPrice.getMarketCap());
      }

      @Override
      public int getBatchSize() {
        return historicalPrices.size();
      }
    });
  }

  public int insertCoinDetail(CoinDetails coinDetails) {
    String sql = "INSERT INTO Coin (coinName, descirption, Symbol) values (?,?, ?)";

    final PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
      @Override
      public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
        final PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

        preparedStatement.setString(1, coinDetails.getName());
        preparedStatement.setString(2, coinDetails.getDescription());
        preparedStatement.setString(3, coinDetails.getSymbol());

        return preparedStatement;
      }
    };

    final KeyHolder keyHolder = new GeneratedKeyHolder();

    jdbcTemplate.update(preparedStatementCreator, keyHolder);
    return keyHolder.getKey().intValue();
  }

  public void insertRealTimePrice(List<CoinMarketCapCurrency> currencies) {
    jdbcTemplate.batchUpdate("INSERT INTO PriceData_5min (refID_coin, marketCap, price, circulatingSupply, volume24h, movement1h, movement24h, movement7d) values (?, ?, ?, ?, ?, ?, ?, ?)", new BatchPreparedStatementSetter() {
      @Override
      public void setValues(PreparedStatement ps, int i) throws SQLException {
        CoinMarketCapCurrency currency = currencies.get(i);

        Integer coinID = dataOutput.getCoinPerName(currency.getWebsiteID());
        if (coinID == null) {
          coinID = insertCoinDetail(new CoinDetails(currency.getWebsiteID(), currency.getName(), currency.getSymbol()));
        }

        ps.setInt(1, coinID);
        ps.setLong(2, currency.getMarketCap());
        ps.setDouble(3, currency.getPrice());
        ps.setLong(4, currency.getCirculatingSupply());
        ps.setLong(5, currency.getVolume_24h());
        ps.setDouble(6, currency.getMovement_1h());
        ps.setDouble(7, currency.getMovement_24h());
        ps.setDouble(8, currency.getMovement_7d());
      }

      @Override
      public int getBatchSize() {
        return currencies.size();
      }
    });
  }

  public void insertRealTimeMarketsData(List<MarketDetailsData> marketDetailsData){
    jdbcTemplate.batchUpdate("INSERT INTO marketDetails (refID_Coin, refID_Market, refID_CurrencyPair, volume_24h, price, volumeInPercent) VALUES (?, ?, ?, ?, ?, ?)", new BatchPreparedStatementSetter() {
      @Override
      public void setValues(PreparedStatement ps, int i) throws SQLException {
        MarketDetailsData marketDetailsData1 = marketDetailsData.get(i);

        Integer coinID = dataOutput.getCoinPerDescription(marketDetailsData1.getCurrencyName());
        if(coinID != null){
          Integer currencyPairID = dataOutput.getCurrencyPairPerName(marketDetailsData1.getCurrencyPair());
          if(currencyPairID == null){
            currencyPairID = insertCurrencyPairData(marketDetailsData1.getCurrencyPair());
          }
          ps.setInt(1,coinID);
          ps.setInt(2,marketDetailsData1.getCurrencyMarketID());
          ps.setInt(3,currencyPairID);
          ps.setLong(4,marketDetailsData1.getVolume_24h());
          ps.setDouble(5,marketDetailsData1.getPrice());
          ps.setDouble(6,marketDetailsData1.getVolumeper());
        }
      }

      @Override
      public int getBatchSize() {
        return marketDetailsData.size();
      }
    });
  }

  public void insertMarketDetails(List<MarketsDetails> marketsDetails) {
    if (marketsDetails != null) {
      for (int i = 0; i < marketsDetails.size(); i++) {
        MarketsDetails marketsDetail = marketsDetails.get(i);

        Integer marketID = dataOutput.getMarketPerName(marketsDetail.getName());
        if (marketID == null) {
          marketID = insertMarketDetail(new MarketsDetails(marketsDetail.getName(), marketsDetail.getCoinMarketURL()));
        }
      }
      ;
    }
  }
}
