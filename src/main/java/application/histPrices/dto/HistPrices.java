package application.histPrices.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Date;

/**
 * Created by Niki on 16/01/2018.
 */
public class HistPrices {
  Date date;
  Integer refID_coin;
  Double openPrice;
  Double closePrice;
  Double highPrice;
  Double lowPrice;
  Long volume;
  Long mcap;
}
