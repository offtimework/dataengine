package application.histPrices;

import application.dataFeed.util.HistoricalPricesRowMapper;
import application.histPrices.dto.HistPrices;
import application.scraping.HistoricalPrices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * Created by Niki on 16/01/2018.
 */

@Component
public class HistPricesRepository {
  @Autowired
  private JdbcTemplate jdbcTemplate;

  public List<HistoricalPrices> fetchPrices(Integer coinID){
    String sql = "SELECT c.coinName, date, refID_coin, openPrice, closePrice, highPrice, lowPrice, volume, mcap FROM priceData_Hist h JOIN coin c ON h.refID_coin = c.id WHERE refID_coin = ?";
    return jdbcTemplate.query(sql, new Object [] {coinID}, new HistoricalPricesRowMapper());
  }



}
