package application.histPrices;

import application.histPrices.dto.HistPrices;
import application.scraping.HistoricalPrices;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Niki on 16/01/2018.
 */

@Service
@JsonSerialize
public class HistPricesService {

  @Autowired
  private HistPricesRepository histPricesRepository;

  List<HistoricalPrices> fetchPrices(Integer coinID){
    List<HistoricalPrices> histPrices = histPricesRepository.fetchPrices(coinID);
    return histPrices;
  }
}
