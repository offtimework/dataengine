package application.histPrices;

import application.histPrices.dto.HistPrices;
import application.scraping.HistoricalPrices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Niki on 16/01/2018.
 */
@RestController
public class HistPricesController {
  @Autowired
  HistPricesService histPricesService;

  @GetMapping(value="/gethistprices/{coinId}")
  List<HistoricalPrices> fetchPrices(
      @PathVariable("coinId") Integer coinId
  ){
    return histPricesService.fetchPrices(coinId);
  }

}
